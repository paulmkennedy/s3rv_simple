`timescale 1ns / 1ps
`default_nettype none

module SevenSegmentDriver #(
    parameter ROTATE = 0
)(
    input  wire        clk,
    input  wire        rst,
    input  wire [15:0] hex_digits,
    output wire  [6:0] ss_abcdefg_l,
    output wire        ss_dp_l,
    output wire  [3:0] ss_sel_l
);

    wire [17:0] clk_cnt_q;
    wire [17:0] clk_cnt_n;
    assign clk_cnt_n = clk_cnt_q + 1;
    Dff #(.W(18)) clk_cnt_reg(.clk(clk), .rst(rst), .d(clk_cnt_n), .q(clk_cnt_q));

    wire [3:0] cycler_q;
    wire [3:0] cycler_n;
    assign cycler_n = (clk_cnt_q == 'b0) ? {cycler_q[2:0], cycler_q[3]} : cycler_q;
    Dff #(.W(4), .RESET_VAL(4'b1)) cycler_reg(.clk(clk), .rst(rst), .d(cycler_n), .q(cycler_q));

    assign ss_sel_l = ~cycler_q;

    reg [3:0] hex_digit;
    always @(*) begin
        case (cycler_q)
            4'b0001: hex_digit = hex_digits[3:0];
            4'b0010: hex_digit = hex_digits[7:4];
            4'b0100: hex_digit = hex_digits[11:8];
            4'b1000: hex_digit = hex_digits[15:12];
            default: hex_digit = 'b0;
        endcase
    end

    assign ss_dp_l = 1'b1;

    HexToSS #(.ROTATE(ROTATE)) hex_to_ss(.hex(hex_digit), .ss_abcdefg_l(ss_abcdefg_l));

endmodule

module HexToSS #(
    parameter ROTATE=0
)(
    input wire [3:0] hex,
    output reg [6:0] ss_abcdefg_l
);

    always @(hex) begin
        case (hex)
            4'h0: ss_abcdefg_l = ROTATE ? 7'b0000001 : 7'b0000001;
            4'h1: ss_abcdefg_l = ROTATE ? 7'b1111001 : 7'b1001111;
            4'h2: ss_abcdefg_l = ROTATE ? 7'b0010010 : 7'b0010010;
            4'h3: ss_abcdefg_l = ROTATE ? 7'b0110000 : 7'b0000110;
            4'h4: ss_abcdefg_l = ROTATE ? 7'b1101000 : 7'b1001100;
            4'h5: ss_abcdefg_l = ROTATE ? 7'b0100100 : 7'b0100100;
            4'h6: ss_abcdefg_l = ROTATE ? 7'b0000100 : 7'b0100000;
            4'h7: ss_abcdefg_l = ROTATE ? 7'b1110001 : 7'b0001111;
            4'h8: ss_abcdefg_l = ROTATE ? 7'b0000000 : 7'b0000000;
            4'h9: ss_abcdefg_l = ROTATE ? 7'b1100000 : 7'b0001100;
            4'ha: ss_abcdefg_l = ROTATE ? 7'b1000000 : 7'b0001000;
            4'hb: ss_abcdefg_l = ROTATE ? 7'b0001100 : 7'b1100000;
            4'hc: ss_abcdefg_l = ROTATE ? 7'b0000111 : 7'b0110001;
            4'hd: ss_abcdefg_l = ROTATE ? 7'b0011000 : 7'b1000010;
            4'he: ss_abcdefg_l = ROTATE ? 7'b0000110 : 7'b0110000;
            4'hf: ss_abcdefg_l = ROTATE ? 7'b1000110 : 7'b0111000;
            default: ss_abcdefg_l = 7'b1111111;
        endcase
    end

endmodule
