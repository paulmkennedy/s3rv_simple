`timescale 1ns / 1ps
`default_nettype none

module S3rv(
    input  wire         clk,

    input  wire [3:0]   buttons,
    input  wire [7:0]   switches,
    output wire [7:0]   leds,

    output wire [6:0]   ss_abcdefg_l,
    output wire         ss_dp_l,
    output wire [3:0]   ss_sel_l,

    output wire [2:0]   vga_rgb,
    output wire         vga_hs_l,
    output wire         vga_vs_l,

    output wire [17:0]  sram_addr,
    output wire         sram_oe_l,
    output wire         sram_we_l,

    inout  wire [15:0]  sram_data1,
    output wire         sram_ce1_l,
    output wire         sram_ub1_l,
    output wire         sram_lb1_l,

    inout  wire [15:0]  sram_data2,
    output wire         sram_ce2_l,
    output wire         sram_ub2_l,
    output wire         sram_lb2_l
);

    // TODO: on reset, copy from block RAM into SRAM

    // Counter program:
    //    li x1, 0
    // 1: addi x1, 1
    //    j 1b
    //
    // 0000 00b3
    // 0010 8093
    // ffdf f06f
    //
    // Fibonacci program:
    //    li x1, 1
    //    li x2, 1
    // 1: add x1, x1, x2
    //    add x2, x1, x2
    //    j 1b
    //
    // 0010 0093
    // 0010 0113
    // 0020 80b3
    // 0020 8133
    // ff9f f06f

    assign vga_rgb = 3'b0;
    assign vga_hs_l = 1'b1;
    assign vga_vs_l = 1'b1;

    wire sram_en;
    assign sram_en = 1'b1; // TODO

    wire rst;
    assign rst = buttons[3];

    // button debouncing
    wire [3:0] buttons_q, buttons_q2;
    Dff #(.W(4)) buttons_ffs1(.clk(clk), .rst(rst), .d(buttons), .q(buttons_q));
    Dff #(.W(4)) buttons_ffs2(.clk(clk), .rst(rst), .d(buttons_q), .q(buttons_q2));

    wire [3:0] buttons_db, buttons_db_q;
    Debouncer #(.W(4)) debouncer(.clk(clk), .rst(rst), .in(buttons_q2), .out(buttons_db));
    Dff #(.W(4)) buttons_db_reg(.clk(clk), .rst(rst), .d(buttons_db), .q(buttons_db_q));

    wire [3:0] buttons_pulse;
    assign buttons_pulse = buttons_db & ~buttons_db_q;
    // end

    wire core_clk;
    assign core_clk = buttons_pulse[0];
    //assign core_clk = clk;

    wire [31:0]  addr;
    wire [31:0]  data;
    wire         read;
    wire         write;
    wire [1:0]   size;
    wire [4:0]   reg_sel;
    wire [31:0]  reg_out;

    assign reg_sel = switches[4:0];
    assign leds = reg_out[7:0];

    SevenSegmentDriver seven_segment_driver(
        .clk         (clk),
        .rst         (rst),
        .hex_digits  (reg_out),
        .ss_abcdefg_l(ss_abcdefg_l),
        .ss_dp_l     (ss_dp_l),
        .ss_sel_l    (ss_sel_l)
    );

    Core core(
        .clk    (core_clk),
        .rst    (rst),
        .addr   (addr),
        .data   (data),
        .read   (read),
        .write  (write),
        .size   (size),
        .reg_sel(reg_sel),
        .reg_out(reg_out)
    );

    // Little endian
    // Register: 0xdeadbeef
    // Memory:
    //  00: 0xef LB1
    //  01: 0xbe UB1
    //  10: 0xad LB2
    //  11: 0xde UB2
    //
    //   22 22 11 11            CE_L LB_L UB_L
    //   UB LB UB LB     SZ AA  1 2  1 2  1 2
    //  [-- -- -- 00] 1b 11 00  0 1  0 -  1 -
    //  [-- -- 01 --] 1b 11 01  0 1  1 -  0 -
    //  [-- -- 01 00] 2b 10 00  0 1  0 -  0 -
    //  [-- 10 -- --] 1b 11 10  1 0  - 0  - 1
    //  [11 -- -- --] 1b 11 11  1 0  - 1  - 0
    //  [11 10 -- --] 2b 10 10  1 0  - 0  - 0
    //  [11 10 01 00] 4b 00 00  0 0  0 0  0 0

    assign sram_addr  = addr[19:2];
    assign sram_oe_l  = ~(sram_en & read);
    assign sram_we_l  = ~(sram_en & write);
    assign sram_data1 = (~sram_we_l && ~sram_ce1_l) ? data[15:0]  : 'bz;
    assign sram_data2 = (~sram_we_l && ~sram_ce2_l) ? data[31:16] : 'bz;

    assign data[15:0] = (~sram_oe_l && ~sram_ce1_l) ? sram_data1 : 'bz;
    assign data[31:16]= (~sram_oe_l && ~sram_ce2_l) ? sram_data2 : 'bz;

    wire [1:0] addr_aligned;
    assign addr_aligned = addr[1:0] & size;

    assign sram_ce1_l = ~(sram_en & ~addr_aligned[1]);
    assign sram_ce2_l = ~(sram_en & (addr_aligned[1] | ~size[1]));
    assign sram_lb1_l = addr_aligned[0];
    assign sram_ub1_l = ~(addr_aligned[0] | ~size[0]);
    assign sram_lb2_l = addr_aligned[0];
    assign sram_ub2_l = ~(addr_aligned[0] | ~size[0]);

endmodule
