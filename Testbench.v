`timescale 1ns / 1ps
`default_nettype none

module Testbench;

    reg clk;
    reg rst;

    wire [3:0]   buttons;
    reg  [7:0]   switches;
    wire [7:0]   leds;

    wire [6:0]   ss_abcdefg_l;
    wire         ss_dp_l;
    wire [3:0]   ss_sel_l;

    wire [2:0]   vga_rgb;
    wire         vga_hs_l;
    wire         vga_vs_l;

    wire [17:0]  sram_addr;
    wire         sram_oe_l;
    wire         sram_we_l;

    wire [15:0]  sram_data1;
    wire         sram_ce1_l;
    wire         sram_ub1_l;
    wire         sram_lb1_l;

    wire [15:0]  sram_data2;
    wire         sram_ce2_l;
    wire         sram_ub2_l;
    wire         sram_lb2_l;

    assign buttons[3] = rst;
    assign buttons[2:0] = 2'b0;

    S3rv s3rv(
        .clk         (clk),
        .buttons     (buttons),
        .switches    (switches),
        .leds        (leds),
        .ss_abcdefg_l(ss_abcdefg_l),
        .ss_dp_l     (ss_dp_l),
        .ss_sel_l    (ss_sel_l),
        .vga_rgb     (vga_rgb),
        .vga_hs_l    (vga_hs_l),
        .vga_vs_l    (vga_vs_l),
        .sram_addr   (sram_addr),
        .sram_oe_l   (sram_oe_l),
        .sram_we_l   (sram_we_l),
        .sram_data1  (sram_data1),
        .sram_ce1_l  (sram_ce1_l),
        .sram_ub1_l  (sram_ub1_l),
        .sram_lb1_l  (sram_lb1_l),
        .sram_data2  (sram_data2),
        .sram_ce2_l  (sram_ce2_l),
        .sram_ub2_l  (sram_ub2_l),
        .sram_lb2_l  (sram_lb2_l)
    );

    Sram #("ram1.txt") ic10(.addr(sram_addr), .data(sram_data1), .oe_l(sram_oe_l), .we_l(sram_we_l), .ce_l(sram_ce1_l), .ub_l(sram_ub1_l), .lb_l(sram_lb1_l));
    Sram #("ram2.txt") ic11(.addr(sram_addr), .data(sram_data2), .oe_l(sram_oe_l), .we_l(sram_we_l), .ce_l(sram_ce2_l), .ub_l(sram_ub2_l), .lb_l(sram_lb2_l));

    always #10 clk = ~clk;

    initial begin

        clk = 0;
        rst = 0;
        switches = 'b0;

        // Wait 100 ns for global reset to finish
        #100;

        @(posedge clk) rst <= 1'b1;
        @(posedge clk) rst <= 1'b0;

        repeat (90) begin
            @(posedge clk);
            $display("fetch        %h: %h", sram_addr, {sram_data2, sram_data1});
            @(posedge clk);
            if (!sram_oe_l) begin
                $display("update read  %h: %h", sram_addr, {sram_data2, sram_data1});
            end
            if (!sram_we_l) begin
                $display("update write %h: %h", sram_addr, {sram_data2, sram_data1});
            end
        end

        $finish;

    end

`define INST_EBREAK 6'd41
`define S_FETCH     1'd0

    always @(*) begin
        if ((s3rv.core.state_q == `S_FETCH) && (s3rv.core.decode.inst == `INST_EBREAK)) begin
            $display("EBREAK %t", $time);
            $finish;
        end
    end

endmodule
