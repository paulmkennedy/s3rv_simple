`timescale 1ns / 1ps
`default_nettype none

module Debouncer #(
    parameter W = 1,
    parameter TIMER_BITS = 18
)(
    input  wire         clk,
    input  wire         rst,
    input  wire [W-1:0] in,
    output wire [W-1:0] out
);

    wire [TIMER_BITS-1:0] timer_n, timer_q;
    assign timer_n = timer_q + 1;
    Dff #(.W(TIMER_BITS)) timer(.clk(clk), .rst(rst), .d(timer_n), .q(timer_q));

    wire [W-1:0] out_n;
    assign out_n = (timer_q == 0) ? in : out;
    Dff #(.W(W)) out_reg(.clk(clk), .rst(rst), .d(out_n), .q(out));

endmodule
